FirstQuery

SELECT
  dbo.Groups.Name,
  COUNT(dbo.Groups.Group_ID)AS StudentsCount
FROM dbo.Students 
  JOIN dbo.Groups 
  ON dbo.Students.Group_ID = dbo.Groups.Group_ID
  GROUP BY 
  dbo.Groups.Name
HAVING 
COUNT(dbo.Groups.Group_ID)<10


SecondQuery

USE Universety
DELETE FROM dbo.Students
FROM 
dbo.Students 
JOIN dbo.Groups
ON dbo.Students.Group_ID = dbo.Groups.Group_ID
WHERE
dbo.Groups.Name LIKE 'SR-01'

ThirdQuery

USE Universety
SELECT
  dbo.Courses.Name AS CourseName,
  dbo.Students.First_Name
FROM
  dbo.Students
  JOIN dbo.Groups
  ON dbo.Students.Group_ID = dbo.Groups.Group_ID
  JOIN dbo.Courses
  ON dbo.Courses.Course_ID =dbo.Groups.Course_ID