IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'Universety')
BEGIN
    CREATE DATABASE [Universety]
END
GO
  USE [Universety]
GO
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='Students' and xtype ='U')
BEGIN
 CREATE TABLE [Students] (
	Student_ID integer NOT NULL,
	Group_ID integer NOT NULL,
	First_Name varchar(255) NOT NULL,
	Last_Name varchar(255) NOT NULL,
  CONSTRAINT [PK_STUDENTS] PRIMARY KEY CLUSTERED
  (
  [Student_ID] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
END
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='Groups' and xtype ='U')
BEGIN
CREATE TABLE [Groups] (
	Group_ID integer NOT NULL,
	Course_ID integer NOT NULL,
	Name varchar(255) NOT NULL,
  CONSTRAINT [PK_GROUPS] PRIMARY KEY CLUSTERED
  (
  [Group_ID] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
END
IF NOT EXISTS(SELECT * FROM sysobjects WHERE name='Courses' and xtype ='U')
BEGIN
CREATE TABLE [Courses] (
	Course_ID integer NOT NULL,
	Name varchar(255) NOT NULL,
	Description varchar(255) NOT NULL,
  CONSTRAINT [PK_COURSES] PRIMARY KEY CLUSTERED
  (
  [Course_ID] ASC
  ) WITH (IGNORE_DUP_KEY = OFF)

)
END
GO
ALTER TABLE [Students] WITH CHECK ADD CONSTRAINT [Students_fk0] FOREIGN KEY ([Group_ID]) REFERENCES [Groups]([Group_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [Students] CHECK CONSTRAINT [Students_fk0]
GO

ALTER TABLE [Groups] WITH CHECK ADD CONSTRAINT [Groups_fk0] FOREIGN KEY ([Course_ID]) REFERENCES [Courses]([Course_ID])
ON UPDATE CASCADE
GO
ALTER TABLE [Groups] CHECK CONSTRAINT [Groups_fk0]
GO